﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp_Bookstore
{
    /// <summary>
    /// BookList is a derived class that is inherited from the Book class.
    /// </summary>
    public class BookList : Book
    {
        /// <summary>
        /// The default constructor of the BookList class.
        /// </summary>
        public BookList() { }

        /// <summary>
        /// These are arrays of the books to stores information about the books.
        /// </summary>
        public static string[] name = { "Tanrı'yı Gören Köpek", "Ah Biz Eşekler", "Sofie'nin Dünyası", "50 Soruda Yapay Zeka", "Hayvan Çiftliği", "Tüfek, Mikrop ve Çelik" };
        public static string[] id = { "ID-001", "ID-002", "ID-003", "ID-004", "ID-005", "ID-006" };
        public static double[] price = { 18.20, 16.08, 35.88, 18, 10, 30.5 };
        public static string[] isbn = { "9789750726415", "9789759038076", "9789758434572", "9786055888589", "9789750741630", "9786052994573" };
        public static int[] page = { 521, 144, 591, 188, 240, 664 };
        public static string[] author = { "Dino Buzzati", "Aziz Nesin", "Jostein Gaarder", "Cem Say", "George Orwell", "Jared Diamond " };
        public static string[] publisher = { "Can Yayınları", "Nesin Yayınevi", "Pan Yayıncılık", "Bilim ve Gelecek Yayınları", "Can Yayınları", "Pegasus Yayınları" };
        public static string[] image = { AppDomain.CurrentDomain.BaseDirectory + "B_1.jpg", AppDomain.CurrentDomain.BaseDirectory + "B_2.jpg", AppDomain.CurrentDomain.BaseDirectory + "B_3.jpg", AppDomain.CurrentDomain.BaseDirectory + "B_4.jpg", AppDomain.CurrentDomain.BaseDirectory + "B_5.jpg", AppDomain.CurrentDomain.BaseDirectory + "B_6.jpg" };

        /// <summary>
        /// This a constructor with parameters.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="id"></param>
        /// <param name="price"></param>
        /// <param name="isbn"></param>
        /// <param name="page"></param>
        /// <param name="author"></param>
        /// <param name="publisher"></param>
        public BookList(string name, string id, double price, string isbn, int page, string author, string publisher) : base(name, id, price, isbn, page, author, publisher){ }

    }
    


}
