﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp_Bookstore
{
    public partial class BookStorePage : Form
    {
        int i = 0;

        /// <summary>
        /// The default constructor of the BookStorePage.
        /// </summary>
        public BookStorePage()
        {
            InitializeComponent();
        }

        /// <summary>
        ///  This function creates an object of the BookForm. It hides the BookStorePage and shows any form 
        ///  according to the selected index of the Combobox.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void showBtn_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex == 0)
            {
                BookForm book = new BookForm();
                book.Show();
                this.Hide();
            }

            else if (comboBox1.SelectedIndex == 1)
            {
                MagazineForm magazine = new MagazineForm();
                magazine.Show();
                this.Hide();
            }

            else if (comboBox1.SelectedIndex == 2)
            {
                MusicCDForm music = new MusicCDForm();
                music.Show();
                this.Hide();
            }
        }


        /// <summary>
        /// This function is used for the design of the BookStorePage.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BookPage_Load(object sender, EventArgs e)
        {
            timer1.Interval = 1000;

            timer1.Enabled = true;
            pictureBox3.Image = ımageList1.Images[0];


            // The form name, username, and date are shown on the form border.
            this.Text += "".PadRight(15) + "Dear " + LoginBookStore.username + ",  welcome to Daylight BookStore";
            this.Text += "".PadRight(15);
            this.Text += DateTime.Now.ToString();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            i++;
            if (i == 2)
            {
                i = 0;
            }
            pictureBox3.Image = ımageList1.Images[i];
        }
    }
}
