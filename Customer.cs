﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WindowsFormsApp_Bookstore
{
    /// <summary>
    /// Customer is a class that includes properties of users.
    /// </summary>
    class Customer
    {
        /// <summary>
        /// These are getter and setter methods of the customers.
        /// </summary>
        public string telephone { get; set; }
        public string customerid { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string email { get; set; }
        public string userName { get; set; }
        public string password { get; set; }



        // Singleton design pattern is used there.
        private static Customer singleton;

        /// <summary>
        /// The default constructor of the Customer class. It is defined as private so that it cannot be invoked by any object.
        /// </summary>
        private Customer() { }

        // This method is used to create only one object of this class.
        public static Customer getCustomer()
        {
            if (singleton == null)
            {
                singleton = new Customer();
            }
            return singleton;
        }


        
        SqlConnection connect = LoginBookStore.connect;

        /// <summary>
        /// This function is used to indicate the customer details.
        /// </summary>
        /// <returns></returns>
        public string printCustomerDetails()
        {
            connect.Open();

            SqlCommand command = new SqlCommand("Select *from Costumer_info ", connect);
            SqlDataReader reader = command.ExecuteReader();


            while (reader.Read())
            {
                if (LoginBookStore.username == reader["username"].ToString().TrimEnd())
                {
                    name = reader["name"].ToString().TrimEnd();
                    customerid = reader["customer_id"].ToString().TrimEnd();
                    address = reader["address"].ToString().TrimEnd();
                    email = reader["email"].ToString().TrimEnd();
                    telephone = reader["phone_number"].ToString().TrimEnd();
                }
            }

            connect.Close();

            return "Customer name: " + name + Environment.NewLine + "Customer ID: " + customerid +
                Environment.NewLine + "Email: " + email + Environment.NewLine + "Phone Number: " + telephone +
                Environment.NewLine + "Address: " + address;

        }


        /// <summary>
        /// This function is used to register a new user.
        /// </summary>
        public void saveCustomer()
        {
            connect.Open();

            SqlCommand command = new SqlCommand("Insert into Costumer_info (customer_id, name, address, email, " +
                "username," + "password, phone_number) values ('" + customerid + "','" + name + "','" + address +
                "','" + email + "','" + userName + "','" + password + "','" + telephone + "')", connect);

            command.ExecuteNonQuery();

            MessageBox.Show("Your registration is complete.");


            connect.Close();
        }


        /// <summary>
        /// This function is used to indicate the purchases of the customer.
        /// </summary>
        /// <returns></returns>
        public string printCustomerPurchases()
        {
            string purchase = "";
            for (int i = 0; i < CartForm.quantityList.Count; i++)
            {
                purchase += CartForm.nameList[i] + Environment.NewLine;
            }
            return purchase;
        }
    }
}
