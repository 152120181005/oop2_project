﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp_Bookstore
{
    public partial class BookInfo : Form
    {
        /// <summary>
        /// The default constructor of the BookInfo.
        /// </summary>
        public BookInfo()
        {
            InitializeComponent();
        }

        // i is a variable that is equalized to the static variable of BookForm.
        int i = BookForm.index;


        /// <summary>
        /// It indicates the properties of any selected book.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BookInfo_Load(object sender, EventArgs e)
        {
            // An object of the Book class is created.
            Book book = new Book();

            // The information of the books is transferred from the BookList class to the Book class.
            book.Name = BookList.name[i].ToString();
            book.ID = BookList.id[i].ToString();
            book.Price = BookList.price[i];
            book.ISBN = BookList.isbn[i];
            book.Author = BookList.author[i].ToString();
            book.Publisher = BookList.publisher[i].ToString();
            book.Page = BookList.page[i];
            Book_pictureBox.ImageLocation = BookList.image[i].ToString();


            // The properties of the selected product are shown.
            info_lbl.Text = book.PrintDetailsProperties();


            // The form name, username, and date are shown on the form border.
            this.Text += "".PadRight(15) + "Dear " + LoginBookStore.username + ",  welcome to Daylight BookStore";
            this.Text += "".PadRight(20);
            this.Text += DateTime.Now.ToString();
        }


        /// <summary>
        /// This function creates an object of the BookForm. It hides the BookInfo and shows the BookForm.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BackBtn_Click(object sender, EventArgs e)
        {
            BookForm frm = new BookForm();
            frm.Show();
            this.Hide();
        }

        /// <summary>
        /// This function provides to add the selected product to the shopping cart.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddBtn_Click(object sender, EventArgs e)
        {
            int number = Convert.ToInt32(Amount_numericUpDown.Value);
            CartForm.quantityList.Add("Book");
            CartForm.nameList.Add(BookList.name[i] + " X " + number.ToString());
            CartForm.priceList.Add(BookList.price[i] * Convert.ToDouble(number));


            CartForm c = new CartForm();
            c.Show();
            BookInfo bookInfo = this;
            bookInfo.Hide();
        }
    }
}
