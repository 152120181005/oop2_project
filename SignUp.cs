﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WindowsFormsApp_Bookstore
{
    public partial class SignUp : Form
    {
        /// <summary>
        /// The default constructor of SignUp.
        /// </summary>
        public SignUp()
        {
            InitializeComponent();
        }

        SqlConnection connect = LoginBookStore.connect;

        // The object of the customer is invoked here.
        Customer customer = Customer.getCustomer();


        /// <summary>
        /// This function is used to register a new user.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SignUpBtn_Click(object sender, EventArgs e)
        {
            int flag = 0;

            customer.customerid = ID_textBox.Text;
            customer.name = Name_textBox.Text;
            customer.address = Adress_textBox.Text;
            customer.email = Email_textBox.Text;
            customer.userName = Username_textBox.Text;
            customer.password = Password_textBox.Text;
            customer.telephone = Phone_textBox.Text;


            connect.Open();

            SqlCommand command = new SqlCommand("Select *from Costumer_info ", connect);

            SqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                if (ID_textBox.Text == reader["customer_id"].ToString().TrimEnd())
                {
                    flag = 1;
                }
                else
                {
                    flag = 0;
                    
                }
            }
            connect.Close();

            if(flag==0)
            {
                customer.saveCustomer();
                LoginBookStore Lgn = new LoginBookStore();
                Lgn.Show();
                this.Hide();
            }
            else
                MessageBox.Show("This information belongs to another user!");
        }

        private void SignUp_Load(object sender, EventArgs e)
        {
            // The form name, username, and date are shown on the form border.
            this.Text += "".PadRight(15) + "Dear " + LoginBookStore.username + ",  welcome to Daylight BookStore";
            this.Text += "".PadRight(20);
            this.Text += DateTime.Now.ToString();
        }
    }
}
