﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp_Bookstore
{
    public partial class BookForm : Form
    {
        /// <summary>
        /// The default constructor of the BookForm.
        /// </summary>
        public BookForm()
        {
            InitializeComponent();
        }

        // index is a static integer.
        public static int index = 0;
        Book b = new Book();

        /// <summary>
        /// This function creates an object of the BookInfo. It hides the BookForm and shows the BookInfo.
        /// Also, it equalized the index to 1.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void book2_linklbl_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            index = 1;
            BookInfo info = new BookInfo();
            info.Show();
            this.Hide();
        }

        /// <summary>
        /// It indicates all of the books in the online grocery.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BookForm_Load(object sender, EventArgs e)
        {
            // The form name, username, and date are shown on the form border.
            this.Text += "".PadRight(15) + "Dear " + LoginBookStore.username + ",  welcome to Daylight BookStore";
            this.Text += "".PadRight(20);
            this.Text += DateTime.Now.ToString();


            // The object of the Book class is invoked here.
            ProductCreater creater = new ProductCreater();
            Product book = creater.FactoryMethod(Products.Book);

            // The information of the books is transferred from the BookList class to the Book class.
            book.Name = BookList.name[0];
            book.ID = BookList.id[0];
            book.Price = BookList.price[0];
            book1_lbl.Text = book.PrintProperties();

            book.Name = BookList.name[1];
            book.ID = BookList.id[1];
            book.Price = BookList.price[1];
            book2_lbl.Text = book.PrintProperties();

            book.Name = BookList.name[2];
            book.ID = BookList.id[2];
            book.Price = BookList.price[2];
            book3_lbl.Text = book.PrintProperties();

            book.Name = BookList.name[3];
            book.ID = BookList.id[3];
            book.Price = BookList.price[3];
            book4_lbl.Text = book.PrintProperties();

            book.Name = BookList.name[4];
            book.ID = BookList.id[4];
            book.Price = BookList.price[4];
            book5_lbl.Text = book.PrintProperties();

            book.Name = BookList.name[5];
            book.ID = BookList.id[5];
            book.Price = BookList.price[5];
            book6_lbl.Text = book.PrintProperties();
        }

        /// <summary>
        /// This function creates an object of the BookInfo. It hides the BookForm and shows the BookInfo.
        /// Also, it equalized the index to 0.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void book1_linklbl_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            index = 0;
            BookInfo info = new BookInfo();
            info.Show();
            this.Hide();
        }


        /// <summary>
        /// This function creates an object of the BookInfo. It hides the BookForm and shows the BookInfo.
        /// Also, it equalized the index to 2.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void book3_linklbl_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            index = 2;
            BookInfo info = new BookInfo();
            info.Show();
            this.Hide();
        }


        /// <summary>
        /// This function creates an object of the BookInfo. It hides the BookForm and shows the BookInfo.
        /// Also, it equalized the index to 3.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void book4_linklbl_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            index = 3;
            BookInfo info = new BookInfo();
            info.Show();
            this.Hide();
        }


        /// <summary>
        /// This function creates an object of the BookInfo. It hides the BookForm and shows the BookInfo.
        /// Also, it equalized the index to 4.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void book5_linklbl_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            index = 4;
            BookInfo info = new BookInfo();
            info.Show();
            this.Hide();
        }


        /// <summary>
        /// This function creates an object of the BookInfo. It hides the BookForm and shows the BookInfo.
        /// Also, it equalized the index to 5.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void book6_linklbl_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            index = 5;
            BookInfo info = new BookInfo();
            info.Show();
            this.Hide();
        }
    }
}
