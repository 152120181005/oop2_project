﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp_Bookstore
{
    /// <summary>
    /// The Product is an abstract class for the products.
    /// </summary>
    public abstract class Product
    {
        /// <summary>
        /// These are getter and setter methods of the products.
        /// </summary>
        public string Name { get; set; }
        public string ID { get; set; }
        public double Price { get; set; }

        /// <summary>
        /// An abstract function to indicate properties of the products.
        /// </summary>
        /// <returns></returns>
        abstract public string PrintProperties();
    }
}
