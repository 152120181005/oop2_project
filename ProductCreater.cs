﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp_Bookstore
{
    // These are types of products.
    enum Products
    {
        Book,
        Magazine,
        MusicCD
    }

    /// <summary>
    /// The ProductCreater provides the object of products.
    /// </summary>
    class ProductCreater
    {
        /// <summary>
        /// The factory design method is used there. A new object is created according to product type.
        /// </summary>
        /// <param name="productType"></param>
        /// <returns></returns>
        public Product FactoryMethod(Products productType)
        {
            Product pro = null;
            switch (productType)
            {
                case Products.Book:
                    pro = new Book();
                    break;
                case Products.Magazine:
                    pro = new Magazine();
                    break;
                case Products.MusicCD:
                    pro = new MusicCD();
                    break;
            }
            return pro;
        }
    }
}
