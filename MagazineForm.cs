﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp_Bookstore
{
    public partial class MagazineForm : Form
    {
        /// <summary>
        /// The default constructor of the MagazineForm.
        /// </summary>
        public MagazineForm()
        {
            InitializeComponent();
        }

        // index is a static integer.
        public static int a;


        /// <summary>
        /// It indicates all of the magazines in the online grocery.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MagazineForm_Load(object sender, EventArgs e)
        {
            // The form name, username, and date are shown on the form border.
            this.Text += "".PadRight(15) + "Dear " + LoginBookStore.username + ",  welcome to Daylight BookStore";
            this.Text += "".PadRight(20);
            this.Text += DateTime.Now.ToString();


            // The object of the Magazine class is invoked here.
            ProductCreater creater = new ProductCreater();
            Product magazine = creater.FactoryMethod(Products.Magazine);

            // The information of the magazines is transferred from the MagazineeList class to the Magazine class.
            magazine.Name = MagazineeList.name[0].ToString();
            magazine.ID = MagazineeList.id[0].ToString();
            magazine.Price = MagazineeList.price[0];
            label1.Text = magazine.PrintProperties();

            magazine.Name = MagazineeList.name[1].ToString();
            magazine.ID = MagazineeList.id[1].ToString();
            magazine.Price = MagazineeList.price[1];
            label2.Text = magazine.PrintProperties();

            magazine.Name = MagazineeList.name[2].ToString();
            magazine.ID = MagazineeList.id[2].ToString();
            magazine.Price = MagazineeList.price[2];
            label3.Text = magazine.PrintProperties();

            magazine.Name = MagazineeList.name[3].ToString();
            magazine.ID = MagazineeList.id[3].ToString();
            magazine.Price = MagazineeList.price[3];
            label4.Text = magazine.PrintProperties();

            magazine.Name = MagazineeList.name[4].ToString();
            magazine.ID = MagazineeList.id[4].ToString();
            magazine.Price = MagazineeList.price[4];
            label5.Text = magazine.PrintProperties();

            magazine.Name = MagazineeList.name[5].ToString();
            magazine.ID = MagazineeList.id[5].ToString();
            magazine.Price = MagazineeList.price[5];
            label6.Text = magazine.PrintProperties();
        }

        /// <summary>
        /// This function creates an object of the MagazineInfo. It hides the MagazineForm and shows the MagazineInfo.
        /// Also, it equalized the index to 0.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void book1_linklbl_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            a = 0;
            MagazineInfo info = new MagazineInfo();
            info.Show();
            this.Hide();
        }

        /// <summary>
        /// This function creates an object of the MagazineInfo. It hides the MagazineForm and shows the MagazineInfo.
        /// Also, it equalized the index to 1.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void book2_linklbl_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            a = 1;
            MagazineInfo info = new MagazineInfo();
            info.Show();
            this.Hide();
        }

        /// <summary>
        /// This function creates an object of the MagazineInfo. It hides the MagazineForm and shows the MagazineInfo.
        /// Also, it equalized the index to 2.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void book3_linklbl_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            a = 2;
            MagazineInfo info = new MagazineInfo();
            info.Show();
            this.Hide();
        }

        /// <summary>
        /// This function creates an object of the MagazineInfo. It hides the MagazineForm and shows the MagazineInfo.
        /// Also, it equalized the index to 3. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void book4_linklbl_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            a = 3;
            MagazineInfo info = new MagazineInfo();
            info.Show();
            this.Hide();
        }

        /// <summary>
        /// This function creates an object of the MagazineInfo. It hides the MagazineForm and shows the MagazineInfo.
        /// Also, it equalized the index to 4. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void book5_linklbl_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            a = 4;
            MagazineInfo info = new MagazineInfo();
            info.Show();
            this.Hide();
        }

        /// <summary>
        /// This function creates an object of the MagazineInfo. It hides the MagazineForm and shows the MagazineInfo.
        /// Also, it equalized the index to 5.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void book6_linklbl_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            a = 5;
            MagazineInfo info = new MagazineInfo();
            info.Show();
            this.Hide();
        }

       
    }
}
