﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp_Bookstore
{
    /// <summary>
    /// MagazineeList is a derived class that is inherited from the Magazine class.
    /// </summary>
    public class MagazineeList : Magazine
    {
        /// <summary>
        /// The default constructor of the MagazineeList class.
        /// </summary>
        public MagazineeList() { }

        /// <summary>
        /// These are arrays of the magazines to stores information about the magazines.
        /// </summary>
        public static string[] name = { "Vogue", "NewTech", "Design Home", "Time", "Popular Science", "Nature" };
        public static string[] id = { "ID-010", "ID-011", "ID-012", "ID-013", "ID-014", "ID-015" };
        public static double[] price = { 20.5, 18.5, 20.8, 15, 27.2, 16 };
        public static string[] issue = { "Latest fashion trends", "Latest developments in technology", "Decorating tips", "Will the coronavirus end?", "Developments in space", "One week with nature" };
        public static string[] type = { "Actuel", "Technology", "Decoration", "News", "Science", "Nature" };
        public static string[] image = { AppDomain.CurrentDomain.BaseDirectory + "M_1.jpg", AppDomain.CurrentDomain.BaseDirectory + "M_2.jpg", AppDomain.CurrentDomain.BaseDirectory + "M_3.jpg", AppDomain.CurrentDomain.BaseDirectory + "M_4.jpg", AppDomain.CurrentDomain.BaseDirectory + "M_5.jpg", AppDomain.CurrentDomain.BaseDirectory + "M_6.jpeg" };


        /// <summary>
        /// This a constructor with parameters.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="id"></param>
        /// <param name="price"></param>
        /// <param name="issue"></param>
        /// <param name="type"></param>
        public MagazineeList(string name, string id, double price, string issue, string type) : base(name, id, price, issue, type) { }

    }
}
