﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp_Bookstore
{
    /// <summary>
    /// Book is a derived class that is inherited from the Product class. 
    /// </summary>
    public class Book : Product
    {
        /// <summary>
        /// These are getter and setter methods of the books.
        /// </summary>
        public int Page { get; set; }
        public string Author { get; set; }
        public string Publisher { get; set; }
        public string ISBN { get; set; }


        /// <summary>
        /// The default constructor of the Book class.
        /// </summary>
        public Book() { }


        /// <summary>
        /// This is a constructor of the Book class. It includes all properties of the books as parameters.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="id"></param>
        /// <param name="price"></param>
        /// <param name="isbn"></param>
        /// <param name="page"></param>
        /// <param name="author"></param>
        /// <param name="publisher"></param>
        public Book(string name, string id, double price, string isbn, int page, string author, string publisher)
        {
            base.Name = name;
            base.ID = id;
            base.Price = price;
            this.ISBN = isbn;
            this.Page = page;
            this.Author = author;
            this.Publisher = publisher;

        }

        /// <summary>
        /// The PrintProperties() method of the Product class is overridden in the Book class.
        /// </summary>
        /// <returns></returns>
        public override string PrintProperties()
        {
            return ID + Environment.NewLine + Name + Environment.NewLine + Price+"$";
        }


        /// <summary>
        /// This method indicates the properties of the books
        /// </summary>
        /// <returns></returns>
        public string PrintDetailsProperties()
        {
            return "Book ID: " + ID + Environment.NewLine + "Book Name: " + Name + Environment.NewLine + "Book Price: " +
                Price + Environment.NewLine + "Book ISBN: " + ISBN + Environment.NewLine + "Book Page: " + Page +
                Environment.NewLine + "Book Author: " + Author + Environment.NewLine + "Book Publisher: " + Publisher;
        }
    }
}
